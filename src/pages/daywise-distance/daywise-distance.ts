import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';
import { ApiServiceProvider } from '../../providers/api-service/api-service';
import { GeocoderProvider } from '../../providers/geocoder/geocoder';

@IonicPage()
@Component({
  selector: 'page-daywise-distance',
  templateUrl: 'daywise-distance.html',
})
export class DaywiseDistancePage {
  fdate: any;
  tdate: any;
  uid: any;
  selectedVehicle: any;
  limit: number = 28;
  page: number = 0;
  detailData: any = []


  constructor(public navCtrl: NavController, public navParams: NavParams,
    public viewCtrl: ViewController, public apiCall: ApiServiceProvider,
    private geocoderApi: GeocoderProvider,) {
    this.fdate = this.navParams.get('fdate');
    this.tdate = this.navParams.get('tdate');
    this.uid = this.navParams.get('uid');
    this.selectedVehicle = this.navParams.get('selectedVehicle')
    console.log("details", this.fdate, this.tdate, this.uid, this.selectedVehicle);
  }

  ngOnInit() {
    this.getDetails();
  }

  getDetails() {
    this.apiCall.startLoading().present();
    this.apiCall.distanceDaywiseData(new Date(this.fdate).toISOString(), new Date(this.tdate).toISOString(), this.uid, this.selectedVehicle, this.limit, this.page)
      .subscribe(data => {
        this.apiCall.stopLoading();
        this.detailData = data;
        let finalArr = []
        let that = this;
        for (let j = 0; j < this.detailData.length; j++) {
          this.clocation_1(this.detailData[j], function (err, succ) {
            if (err) {
              console.log(err);
              j++;
            } else {
              finalArr.push(succ);
              console.log(finalArr);
              j++;
              if (j === that.detailData.length) {
                that.detailData = finalArr
                return that.detailData;
              }
            }
          })
        }
      })
  }

  clocation_1(latlngObj, cb) {
    var outerThis = this;

    let latLng = {
      lat: "0",
      long: "0"
    };

    latLng = {
      lat: latlngObj.startLat ? latlngObj.startLat : 0,
      long: latlngObj.startLng ? latlngObj.startLng : 0
    }
    outerThis.apiCall.getAddress(latLng).subscribe(res => {
      if (res.message == "Address not found in databse") {
        var adata = {
          iden: 'noaddress',
          latlng: latLng
        }
        latlngObj['startAddress'] = adata;
        // cb(null,latlngObj);
        let latLng_1 = {
          lat: "0",
          long: "0"
        };

        latLng_1 = {
          lat: latlngObj.endLat ? latlngObj.endLat : 0,
          long: latlngObj.endLng ? latlngObj.endLng : 0
        }


        outerThis.apiCall.getAddress(latLng_1).subscribe(res => {
          if (res.message == "Address not found in databse") {
            let bdata = {
              iden: 'noaddress',
              latlng: latLng_1
            }

            latlngObj['endAddress'] = bdata;
            cb(null, latlngObj);


          } else {
            latlngObj['endAddress'] = res.address;
            cb(null, latlngObj);
          }

        })

      } else {
        latlngObj['startAddress'] = res.address;
        let latLng_1 = {
          lat: "0",
          long: "0"
        };

        latLng_1 = {
          lat: latlngObj.endLat ? latlngObj.endLat : 0,
          long: latlngObj.endLng ? latlngObj.endLng : 0
        }
        outerThis.apiCall.getAddress(latLng_1).subscribe(res => {
          if (res.message == "Address not found in databse") {
            let cdata = {
              iden: 'noaddress',
              latlng: latLng_1
            }

            latlngObj['endAddress'] = cdata;
            cb(null, latlngObj);


          } else {
            latlngObj['endAddress'] = res.address;
            cb(null, latlngObj);
          }

        })
      }
    })

  }

  start_address(d, index) {
    let that = this;
    let tempcord = {
      "lat": d.startLat,
      "long": d.startLng
    }
    this.apiCall.getAddress(tempcord)
      .subscribe(res => {

        if (res.message == "Address not found in databse") {
          this.geocoderApi.reverseGeocode(d.startLat, d.startLng)
            .then(res => {
              let str = res.replace(/,\s*$/, ""); //removes last quama in the string using regular expression
              that.saveAddressToServer(str, d.startLat, d.startLng);
              that.detailData[index].StartLocation = str;
              // console.log("inside", that.address);
            })
        } else {
          that.detailData[index].StartLocation = res.address;
        }
      })
  }

  end_address(item, index) {
    let that = this;
    if (!item.end_location) {
      that.detailData[index].EndLocation = "N/A";
      return;
    }
    let tempcord = {
      "lat": item.endLat,
      "long": item.endLng
    }
    this.apiCall.getAddress(tempcord)
      .subscribe(res => {
        console.log("test");
        console.log("endlocation result", res);
        console.log(res.address);
        // that.allDevices[index].address = res.address;
        if (res.message == "Address not found in databse") {
          this.geocoderApi.reverseGeocode(item.endLat, item.endLng)
            .then(res => {
              var str = res.replace(/,\s*$/, ""); //removes last quama in the string using regular expression
              that.saveAddressToServer(str, item.endLat, item.endLng);
              that.detailData[index].EndLocation = str;
              // console.log("inside", that.address);
            })
        } else {
          console.log("enter in else")
          that.detailData[index].EndLocation = res.address;
        }
      })
  }

  dismiss() {
    this.viewCtrl.dismiss();
  }

  saveAddressToServer(address, lat, lng) {
    let payLoad = {
      "lat": lat,
      "long": lng,
      "address": address
    }
    this.apiCall.saveGoogleAddressAPI(payLoad)
      .subscribe(respData => {
        console.log("check if address is stored in db or not? ", respData)
      },
        err => {
          console.log("getting err while trying to save the address: ", err);
        });
  }

}
