import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AddDealerPage } from './add-dealer';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  declarations: [
    AddDealerPage,
  ],
  imports: [
    IonicPageModule.forChild(AddDealerPage),
    TranslateModule.forChild()
  ],
})
export class AddDealerPageModule {}
